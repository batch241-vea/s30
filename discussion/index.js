
// MONGODB: AGGREGATION 
/*
	- aggregation is used to grenerate manipulated data and perform operations to create filtered results that helps analyzing data.
	- compared to doing CRUD operations, aggregation fives us access to manipulate, filter, and compute for results. Providing us with information to make necessary development without having to create frontend application.
*/

// Using the aggregated method
/*
	- The "$match" is used to pass the documents that meet the specified condition/s to the next pipeline state/aggregation process:
	- SYntax:
		{$match: {field: "value"} }
		{$group: {_id: "value", fieldResult: "valueResult"}}

	- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group all stocks for all suppliers found.

	- db.collectionName.aggregate([
	{$match: {fieldA: valueA} },
	{$group: {_id: "$fieldB", {result: {operation}} }
	])
*/

db.fruits.aggregate([
{$match:{onSale: true} }, 
{$group: _id: "$supplier_id", total: {$sum: "$stock"}}
	]);


//Field Projection with Aggregation
/*
	- The "$project" can be used when aggregating data to include/exclude fields from the returned results.
	- Syntax:
		{$project: {field 1/0}}
*/
db.fruits.aggregate([
{$match: {onSale: true} },
{$group: {_id: "$supplier_id", total: {$sum: "$stock"} }},
{$project: {_id :0}}
])


// Sorting aggregated results
/*
	- the '$sort' can be used to change the order to aggregat results.
	- proving a value of -1 will sort the aggregated results in reverse order
	- {$sort: {field: -1 / 1}}
*/
db.fruits.aggregate([
{$match: {onSale: true} },
{$group: {_id: "$supplier_id", total: {$sum: "$stock"}} },
{$sort: {total: -1}}
])


// Aggregating results based on array fields
/*
	-	The "$unwind" deconstructs an array field from a collection/field with an array value to aoutput a result for each element
	- THe syntax below will return results, creating separate documents for each of the counties provided per the "origin" field.
	- Syntax:
	{$unwind: "$field" }
*/
db.fruits.aggregate([
{$unwind: "$origin"}
])

// Display fruits documents by their origin and the kind of fruits that are supplied
db.fruits.aggregate([
{$unwind: "$origin"},
{$group: {_id: "$supplier_id", kinds: {$sum: 1}} }
])