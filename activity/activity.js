//
db.fruits.aggregate([
{$match: {onSale: true} },
{$count: "fruitsOnSale" },
{$project: {_id: 0}}
]);


//
db.fruits.aggregate([
{$match: {stock: {$gte: 20}} },
{$count: "enoughStock" },
{$project: {_id: 0}}
]);


//
db.fruits.aggregate(
{$match: {onSale: true} },
{$group: {_id: "$supplier_id", ave_price: {$avg: "$price"}} }
);


//
db.fruits.aggregate(
{$match: {onSale: true} },
{$group: {_id: "$supplier_id", max_price: {$max: "$price"}} }
);


//
db.fruits.aggregate(
{$match: {onSale: true} },
{$group: {_id: "$supplier_id", min_price: {$min: "$price"}} }
);